#include"tracking.h"
#include"ProcessTimeChecker.h"
#include<EdbSegP.h>

#include<EdbPattern.h>
#include<EdbDataSet.h>
#include<TCut.h>
#include<TStopwatch.h>
#include<TEnv.h>
#include<TThread.h>

std::vector<Node*> nodes;
std::vector<Link*> links;

hashtable3d* ht;


float sigmaX2 = 2*2; // BT position resolution, 2 micron.
float sigmaY2 = 2*2; // BT position resolution, 2 micron.
float sigmaMTX = 0.3; // MT position resolution = 0.1 micron.
float sigmaMTZ = 5.0; // MT base thickness resolution = 2 micron.
float thicknessBase = 180.;

float sigmaT = sigmaMTX/thicknessBase;
//float sigmaT2 = 0.005*0.005 + sigmaT*sigmaT; // 5mrad arb
float sigmaT2 = 0.000025 + sigmaT*sigmaT; // 5mrad arb

float sigmaMTZ2_thicknessBase2 = sigmaMTZ*sigmaMTZ/thicknessBase/thicknessBase;

// BT angular resolution is
// Transverse = sigmaMTX/thicknessBase*sqrt(2)
// Longitudinal = Transverse + tanTheta*sigmaMTZ/thicknessBase

// linking parameters
double linkingThreshold = 0.9999; // probability acceptance.
int linkingNdf = 4;
float linkingBranchAcceptance = 0.9; // provability to reject branches. TMath::ChisquareQuantile(linkingBranchAcceptance, linkingNdf)


int iplMax = -100000;
int iplMin =  100000;


float ztable[200];


class segnode : public Node{
	public:
	
	segnode(EdbSegP* s) {
		v[0] = s->Plate();
		v[1] = s->X();
		v[2] = s->Y();
		data = new EdbSegP(*s);
	};
	EdbSegP* getData() { return (EdbSegP*) data;}
	virtual int ID() { return ((EdbSegP*) data)->ID();}
};




void read_nodes_test(char* filename, TCut cut) {
	printf("Read %s, with a cut %s\n", filename, cut.GetTitle());
	
	TFile f(filename);
	TTree* tree = (TTree*) f.Get("tree");
	
	EdbSegP* s = new EdbSegP;
	
	tree->SetBranchAddress("s", &s);
	
	
	TEventList* lst = new TEventList("lst");
	tree->Draw(">>lst",cut,"goff");
	
	printf("selected entries = %d out of %llu\n\n", lst->GetN(), tree->GetEntries());
	
	int n=lst->GetN(); //tree->GetEntries();
	
	for (int i = 0; i < n; i++) {
		tree->GetEntry(lst->GetEntry(i));
		
		segnode* sn = new segnode(s);
		nodes.push_back(sn);
	}
}

void read_nodes_lnkdef(char* filename) {
	
	EdbDataProc* dproc = new EdbDataProc(filename);
	dproc->InitVolume(0);
	EdbPVRec* pvr = dproc->GetPVR();
	
	printf("Read %s\n", filename);
	
	for (int i = 0, Npatt = pvr->Npatterns(); i < Npatt; i++) {
		EdbPattern* pat = pvr->GetPattern(i);
		//printf("ipat = %d\r", i);
		for (int j = 0, patN = pat->GetN(); j < patN; j++) {
			EdbSegP* s = pat->GetSegment(j);
			
			segnode* sn = new segnode(s);
			nodes.push_back(sn);
		}
	}
	delete pvr;
	
}


void read_nodes_linked_tracks() {
	TFile f("D:\\Documents\\DsTau\\m-file\\linked_tracks.root");
	TTree* tracks = (TTree*) f.Get("tracks");
	
	EdbTrackP* t = new EdbTrackP;
	EdbSegP* s = new EdbSegP;
	
	TClonesArray* segs = new TClonesArray("EdbSegP");
	
	tracks->SetBranchAddress("s", &segs);
	
	TCut cut = "1";
	
	TEventList* lst = new TEventList("lst");
	tracks->Draw(">>lst", cut, "goff");
	
	printf("selected entries = %d out of %llu\n\n", lst->GetN(), tracks->GetEntries());
	
	int n = lst->GetN(); //tree->GetEntries();
	
	int total = 0;
	for (int i = 0; i < n; i++) {
		tracks->GetEntry(lst->GetEntry(i));
		
		int nseg = segs->GetEntriesFast();
		total += nseg;
		for (int j = 0; j < nseg; j++) {
			EdbSegP* s1 = (EdbSegP*) (*segs)[j];
			
                        float s1X = s1->X();
                        float s1Y = s1->Y();

			int flag_double = 0;
			for (int k = 0, nodes_size = nodes.size(); k < nodes_size; k++) {
				EdbSegP* s2 = (EdbSegP*) nodes[k]->getData();
				if (fabs(s1X - s2->X()) < 1 && fabs(s1Y - s2->Y()) < 1) {
					flag_double = 1;
					break;
				}
			}
			if (flag_double) continue;
			s1->SetPlate(100 - s1->Plate());
			segnode* sn = new segnode(s1);
			nodes.push_back(sn);
		}
	}
	printf("total %d, selected %zu\n", total, nodes.size());
	
}


void read_nodes() {
	
//	read_nodes_test("segments.root","1");//"abs(s.eX)<50&&abs(s.eY)<50");
	read_nodes_test("D:\\Documents\\DsTau\\m-file\\segments_data.root","1");
//	read_nodes_linked_tracks();
	
	std::stable_sort(nodes.begin(), nodes.end(), Node::compareNodePtr);
	
	printf("%zu segments were read\n", nodes.size());
}

int eval_edge(Node* n1, Node* n2, float* chi2out = NULL, int* ndfout = NULL) {
	static float chi2AcceptanceNdf2 = TMath::ChisquareQuantile(linkingThreshold, 2);
	static float chi2AcceptanceNdf4 = TMath::ChisquareQuantile(linkingThreshold, 4);
	
	segnode* c1 = (segnode*) n1;
	segnode* c2 = (segnode*) n2;
	
	EdbSegP* s1 = (EdbSegP*) c1->getData();
	EdbSegP* s2 = (EdbSegP*) c2->getData();
	
        float s1Z = s1->Z();
        float s2Z = s2->Z();

        float s1TX = s1->TX();
        float s1TY = s1->TY();

        float s2TX = s2->TX();
        float s2TY = s2->TY();

	float chi2 = 0.0;
	int ndf = 0;
	
	// position displacement
	// sigmaX, sigmaY for BT position resolution.
	float middleZ = (s1Z + s2Z)/2;
	float dz = middleZ - s1Z;
	float x1 = s1->X() + s1TX*dz;
	float y1 = s1->Y() + s1TY*dz;
	float x2 = s2->X() + s2TX*(middleZ - s2Z);
	float y2 = s2->Y() + s2TY*(middleZ - s2Z);
	
	float dx = x2 - x1;
	float dy = y2 - y1;
	
	float zunit = dz/abs(s2->Plate() - s1->Plate());
	
	// BT angular resolution is
	// Transverse = sigmaMTX/thicknessBase*sqrt(2)
	// Longitudinal = Transverse + tanTheta*sigmaMTZ/thicknessBase
	
	TVector2 angle1(s1TX, s1TY);
	float phi = angle1.Phi();
	float slope2 = angle1.Mod2();
	
	// Calculate transverse and longitudinal component
	float sigmaL2 = sigmaT2 + slope2*sigmaMTZ2_thicknessBase2;
	
	// Angular displacement of MT from BT
	TVector2 angle1r = angle1.Rotate(-phi); // angle1 rotated
	
	TVector2 angle2(s2TX, s2TY);
	TVector2 angle2r = angle2.Rotate(-phi); // angle2 rotated
	
	TVector2 dangle = angle2r - angle1r;
	
//	float sigmaX2_ = sigmaX2 + sigmaL2*dz*dz*2/2; // conservatively take the effect of angular resolution as longitudinal one.
//	float sigmaY2_ = sigmaY2 + sigmaL2*dz*dz*2/2;
	float sigmaX2_ = sigmaX2 + sigmaL2*800; // 800=20*20*2, conservatively take the effect of angular resolution as longitudinal one.
	float sigmaY2_ = sigmaY2 + sigmaL2*800;

	chi2 += dangle.X()*dangle.X()/sigmaL2 +
	        dangle.Y()*dangle.Y()/sigmaT2 +
	        dx*dx/sigmaX2_ +
	        dy*dy/sigmaY2_;
	ndf += 4;
	
//	if (chi2>chi2AcceptanceNdf2) return 0;
	if (chi2 > chi2AcceptanceNdf4) return 0;
	
	if (chi2out) *chi2out = chi2;
	if (ndfout) *ndfout = ndf;
	
	return 1;
	
}

float eval_path(Link* pth) {
	
	float areaSum = 0;
	int n = 0;
	// 3 point -> area of parallelogram
	for (int i = 0, pns_2 = pth->nodes.size() - 2; i < pns_2; i++) {
		EdbSegP* s1 = (EdbSegP*) pth->nodes[i]->getData();
		EdbSegP* s2 = (EdbSegP*) pth->nodes[i + 1]->getData();
		EdbSegP* s3 = (EdbSegP*) pth->nodes[i + 2]->getData();
		
		TVector3 v1(s1->X(), s1->Y(), s1->Z());
		TVector3 v2(s2->X(), s2->Y(), s2->Z());
		TVector3 v3(s3->X(), s3->Y(), s3->Z());
		TVector3 v12 = v2 - v1;
		TVector3 v13 = v3 - v1;
		TVector3 cross = v12.Cross(v13);
		float area2 = cross.Mag2();
		float area = sqrt(area2);
		areaSum += area;
		n++;
	}
	
	// 2 segment, two x 2 projection areas of parallelogram.
	for (int i = 0, pns_1 = pth->nodes.size() - 1; i < pns_1; i++) {
		EdbSegP* s1 = (EdbSegP*) pth->nodes[i]->getData();
		EdbSegP* s2 = (EdbSegP*) pth->nodes[i + 1]->getData();

                float s1X = s1->X();
                float s1Y = s1->Y();
                float s1Z = s1->Z();

                float s2X = s2->X();
                float s2Y = s2->Y();
                float s2Z = s2->Z();

		TVector3 v1(s1X, s1Y, s1Z);
		TVector3 v2(s2X, s2Y, s2Z);
		TVector3 vMid = (v1 + v2)*0.5;

                float vMidZ = vMid.Z();

		// projection of segment to mid plain.
		TVector3 v1p (s1X + s1->TX()*(vMidZ - s1Z), s1Y + s1->TY()*(vMidZ - s1Z), vMidZ);
		TVector3 v2p (s2X + s2->TX()*(vMidZ - s2Z), s2Y + s2->TY()*(vMidZ - s2Z), vMidZ);
		
		TVector3 cross1 = (v1p - v1).Cross(vMid - v1);
		float area1 = sqrt(cross1.Mag2());
		areaSum += area1;
		TVector3 cross2 = (v2p - v2).Cross(vMid - v2);
		float area2 = sqrt(cross2.Mag2());
		areaSum += area2;
		n++;
	}
	
	areaSum /= n - 0.5;
	
	return areaSum;
	
}


#define NTHREADS 8

struct make_edges_thread_data{
	int ith;
	std::vector<Node*> nodes;
	std::vector<Link*> links;
};

void* make_edges_thread_func(void* ptr) {
	make_edges_thread_data* d = (make_edges_thread_data*) ptr;
	hashtable3d htLocal = *ht;
	std::vector<Node*>& nodesLocal = d->nodes;
	std::vector<Link*>& linksLocal = d->links;
	
	int n = nodesLocal.size();
	
	for (int i = 0; i < n; i++) {
		//if (i%10000 == 0) TThread::Printf("ith %2d, i = %5d / %5d", d->ith, i, n);
		Node* n1 = nodesLocal[i];
		EdbSegP* s1 = (EdbSegP*) n1->getData();
		
		int ipl0 = s1->Plate();
		
		float r[3] = {0.1, 20, 20};
		for (int dpl = 1; dpl <= 3; dpl++) {
			float v[3];
                        int s1Pdpl = s1->Plate() + dpl;
			v[0] = s1Pdpl;
                        float ztable_s1Z = ztable[s1Pdpl] - s1->Z();
			v[1] = s1->X() + s1->TX()*ztable_s1Z;
			v[2] = s1->Y() + s1->TY()*ztable_s1Z;
			
			std::vector<Node*>& neighbors = htLocal.getNeighbors(v, r);
			
			int nn = neighbors.size();
			
			for (int j = 0; j < nn; j++) {
				Node* n2 = neighbors[j];
				if (n1 == n2) continue;
				
				// definition of upstread, downstream
				if (*n1 >= *n2) continue;
				
				// if this pair is within the criteria, accept with a link.
				float chi2;
				if (eval_edge(n1, n2, &chi2) == 1) {
					Link* lnk = new Link(n1, n2);
					lnk->setChi2(chi2);
					linksLocal.push_back(lnk);
					n1->addLink(lnk);
					n2->addLink(lnk);
				}
			}
		}
	}
	
	return 0;
}

void make_edges_multithread() {
	printf("make_edges_multithread()\n");
	int n = nodes.size();
	
	std::vector<Link*> linkstemp;
	
	int nPerThread = (n + NTHREADS)/NTHREADS;
	
	make_edges_thread_data thdata[NTHREADS];
	TObjArray threads;
	for (int ith = 0; ith < NTHREADS; ith++) {
		int offset = ith*nPerThread;
		thdata[ith].ith = ith;
		for (int i = 0; i < nPerThread; i++) {
			int idx = i + offset;
			if (idx >= n) continue;
			thdata[ith].nodes.push_back(nodes[idx]);
		}
		
		TThread* th = new TThread(Form("th%d", ith), make_edges_thread_func, (void*) &thdata[ith]);
		th->Run();
		threads.Add(th);
	}
	
	for (int ith = 0; ith < NTHREADS; ith++) {
		TThread* th = (TThread*) threads.At(ith);
		
		if (th->GetState() == TThread::kRunningState) th->Join();
		
		int nlinks = thdata[ith].links.size();
		for (int i = 0; i < nlinks; i++) {
			linkstemp.push_back(thdata[ith].links[i]);
		}
	}
	
	
	
	printf("%zu nodes\n", nodes.size());
	printf("%zu links\n", linkstemp.size());
	
	printf("cleaning links : reduce branches by means of chi2\n");
	
	// deltaChi2Acceptance is calculated by the accepted probability.
	// The deltaChi2 value is more than a value, the rest will be rejected.
	// for example, for probability of 0.9, the accepted deltaChi2 is
	// delteChi2Acceptance = TMath::ChisquareQuantile(0.9,4) = 7.77;
	// all Link candidates more than deltaChi2=7.77 will be rejected.
	
	float deltaChi2Acceptance = TMath::ChisquareQuantile(linkingBranchAcceptance, linkingNdf);
	
	// branching rejection forward
	for (int inode = 0, nodes_size = nodes.size(); inode < nodes_size; inode++) {

		Node* n1 = nodes[inode];

                int n1_links_size = n1->links.size();

		if (n1_links_size <= 1) continue;
		
		float chi2Min = 1e9;
		for (int ilnk = 0; ilnk < n1_links_size; ilnk++) {
			Link* lnk = n1->links[ilnk];
			if (lnk->flag == Link::REJECTED) continue; // the Link is already rejected
			if (n1 == lnk->nodes.front()) { // this Node is upstream of the link.
				if (chi2Min > lnk->getChi2()) {
					chi2Min = lnk->getChi2();
				}
			}
		}
		for (int ilnk = 0; ilnk < n1_links_size; ilnk++) {
			Link* lnk = n1->links[ilnk];
			if (lnk->flag == Link::REJECTED) continue; // the Link is already rejected
			if (n1 == lnk->nodes.front()) { // this Node is upstream of the link.
				if (lnk->getChi2() - chi2Min > deltaChi2Acceptance) {
					lnk->flag = Link::REJECTED;
				}
			}
		}
	}

	// branching rejection backward
	for (int inode = 0, nodes_size = nodes.size(); inode < nodes_size; inode++) {

		Node* n1 = nodes[inode];

                int n1_links_size = n1->links.size();

		if (n1_links_size <= 1) continue;
		
		float chi2Min = 1e9;
		for (int ilnk = 0; ilnk < n1_links_size; ilnk++) {
			Link* lnk = n1->links[ilnk];
			if (lnk->flag == Link::REJECTED) continue; // the Link is already rejected
			if (n1 == lnk->nodes.back()) { // this Node is downstream of the link.
				if (chi2Min > lnk->getChi2()) {
					chi2Min = lnk->getChi2();
				}
			}
		}
		for (int ilnk = 0; ilnk < n1_links_size; ilnk++) {
			Link* lnk = n1->links[ilnk];
			if (lnk->flag == Link::REJECTED) continue; // the Link is already rejected
			if (n1 == lnk->nodes.back()) { // this Node is downstream of the link.
				if (lnk->getChi2() - chi2Min > deltaChi2Acceptance) {
					lnk->flag = Link::REJECTED;
				}
			}
		}
	}
	
	printf("cleaning links 2: 3 nodes combination\n");
	
	// For a consecutive points A, B, C, D there might be
	// A-B, A-C, A-D,  B-C, B-D,  C-D
	// in order to reduce number of useless links, think a 3 point combination concerning A,
	// like A-B-C, A-C-D, A-B-D, if other links related to are included in this combination, delete them
	// A-C is included in A-B-C and A-C-D, A-D is included in A-B-D,
	// therefore A-C and A-D will be deleted. similary, concerning B, B-D is included in B-C-D.
	// finally only A-B, B-C, C-D will be remain.
	
	for (int inode = 0, nodes_size = nodes.size(); inode < nodes_size; inode++) {
		Node* n1 = nodes[inode];
		if (n1->links.size() <= 1) continue;
		
		for (int ilnk = 0, n1_LS = n1->links.size(); ilnk < n1_LS; ilnk++) {
			Link* lnk = n1->links[ilnk];
			if (lnk->flag == Link::REJECTED) continue; // the Link is already rejected
			if (n1 != lnk->nodes.front()) continue; // this Node is not upstream of the link.
			
			Node* n2 = lnk->nodes.back();
			for (int i = 0, n2_LS = n2->links.size(); i < n2_LS; i++) { // loop on the links of the downstream node.
				Link* lnk2 = n2->links[i];
				if (lnk2->flag == Link::REJECTED) continue; // the Link is already rejected
				Link pth = *lnk; // copy
				pth.addForward(lnk2->nodes.back());
				for (int j = 0, n1_LS = n1->links.size(); j < n1_LS; j++) {
					Link* lnk3 = n1->links[j];
					if (lnk == lnk3) continue;
					if (pth.include(lnk3)) {
						lnk3->flag = Link::REJECTED;
					}
				}
			}
		}
	}
	
	for (int i = 0, nodes_size = nodes.size(); i < nodes_size; i++) {
		Node* n1 = nodes[i];
		for (int j = 0; j < n1->links.size(); ) {
			Link* lnk = n1->links[j];
			if (lnk->flag == Link::REJECTED) n1->removeLink(lnk);
			else j++;
		}
	}
	
	for (int ilnk = 0, lt_size = linkstemp.size(); ilnk < lt_size; ilnk++) {
		Link* lnk = linkstemp[ilnk];
		if (lnk->flag == Link::REJECTED) delete lnk; // rejected links are deleted.
		else links.push_back(lnk);
	}
	
	printf("links cleaning = %zu->%zu\n", linkstemp.size(), links.size());
}
void make_edges() {
	printf("make_edges()\n");
	int n = nodes.size();
	
	std::vector<Link*> linkstemp;
	
	for (int i = 0; i < n; i++) {
		//if (i%10000 == 0) printf("i = %d / %d\r", i, n);
		Node* n1 = nodes[i];
		EdbSegP* s1 = (EdbSegP*) n1->getData();
		
		int ipl0 = s1->Plate();
		
		for (int dpl = 1; dpl <= 3; dpl++) {
			float v[3];
                        int s1Pdpl = s1->Plate() + dpl;
			v[0] = s1Pdpl;
                        float ztable_s1Z = ztable[s1Pdpl] - s1->Z();
			v[1] = s1->X() + s1->TX()*ztable_s1Z;
			v[2] = s1->Y() + s1->TY()*ztable_s1Z;
			
			float r[3] = {0.1, 20, 20};
			std::vector<Node* >& neighbors = ht->getNeighbors(v, r);
			
			int nn = neighbors.size();
			
			for (int j = 0; j < nn; j++) {
				Node* n2 = neighbors[j];
				if (n1 == n2) continue;
				
				// definition of upstread, downstream
				if (*n1 >= *n2) continue;
				
				// if this pair is within the criteria, accept with a link.
				float chi2;
				if (eval_edge(n1, n2, &chi2) == 1) {
					Link* lnk = new Link(n1, n2);
					lnk->setChi2(chi2);
					linkstemp.push_back(lnk);
					n1->addLink(lnk);
					n2->addLink(lnk);
				}
			}
		}
	}
	printf("%zu nodes\n", nodes.size());
	printf("%zu links\n", linkstemp.size());
	
	printf("cleaning links : reduce branches by means of chi2\n");
	
	// deltaChi2Acceptance is calculated by the accepted probability.
	// The deltaChi2 value is more than a value, the rest will be rejected.
	// for example, for probability of 0.9, the accepted deltaChi2 is
	// delteChi2Acceptance = TMath::ChisquareQuantile(0.9,4) = 7.77;
	// all Link candidates more than deltaChi2=7.77 will be rejected.
	
	float deltaChi2Acceptance = TMath::ChisquareQuantile(linkingBranchAcceptance, linkingNdf);
	
	// branching rejection forward
	for (int inode = 0, nodes_size = nodes.size(); inode < nodes_size; inode++) {
		Node* n1 = nodes[inode];

                int n1_links_size = n1->links.size();

		if (n1_links_size <= 1) continue;
		
		float chi2Min = 1e9;
		for (int ilnk = 0; ilnk < n1_links_size; ilnk++) {
			Link* lnk = n1->links[ilnk];
			if (lnk->flag == Link::REJECTED) continue; // the Link is already rejected
			if (n1 == lnk->nodes.front()) { // this Node is upstream of the link.
				if (chi2Min > lnk->getChi2()) {
					chi2Min = lnk->getChi2();
				}
			}
		}
		for (int ilnk = 0; ilnk < n1_links_size; ilnk++) {
			Link* lnk = n1->links[ilnk];
			if (lnk->flag == Link::REJECTED) continue; // the Link is already rejected
			if (n1 == lnk->nodes.front()) { // this Node is upstream of the link.
				if (lnk->getChi2() - chi2Min > deltaChi2Acceptance) {
					lnk->flag = Link::REJECTED;
				}
			}
		}
	}

	// branching rejection backward
	for (int inode = 0, nodes_size = nodes.size(); inode < nodes_size; inode++) {
		Node* n1 = nodes[inode];

                int n1_links_size = n1->links.size();

		if (n1_links_size <= 1) continue;
		
		float chi2Min = 1e9;
		for (int ilnk = 0; ilnk < n1_links_size; ilnk++) {
			Link* lnk = n1->links[ilnk];
			if (lnk->flag == Link::REJECTED) continue; // the Link is already rejected
			if (n1 == lnk->nodes.back()) { // this Node is downstream of the link.
				if (chi2Min > lnk->getChi2()) {
					chi2Min = lnk->getChi2();
				}
			}
		}
		for (int ilnk = 0; ilnk < n1_links_size; ilnk++) {
			Link* lnk = n1->links[ilnk];
			if (lnk->flag == Link::REJECTED) continue; // the Link is already rejected
			if (n1 == lnk->nodes.back()) { // this Node is downstream of the link.
				if (lnk->getChi2() - chi2Min > deltaChi2Acceptance) {
					lnk->flag = Link::REJECTED;
				}
			}
		}
	}
	
	printf("cleaning links 2: 3 nodes combination\n");
	
	// For a consecutive points A, B, C, D there might be
	// A-B, A-C, A-D,  B-C, B-D,  C-D
	// in order to reduce number of useless links, think a 3 point combination concerning A,
	// like A-B-C, A-C-D, A-B-D, if other links related to are included in this combination, delete them
	// A-C is included in A-B-C and A-C-D, A-D is included in A-B-D,
	// therefore A-C and A-D will be deleted. similary, concerning B, B-D is included in B-C-D.
	// finally only A-B, B-C, C-D will be remain.
	
	for (int inode = 0, nodes_size = nodes.size(); inode < nodes_size; inode++) {
		Node* n1 = nodes[inode];

                int n1_LS = n1->links.size();

		if (n1_LS <= 1) continue;
		
		for (int ilnk = 0; ilnk < n1_LS; ilnk++) {
			Link* lnk = n1->links[ilnk];
			if (lnk->flag == Link::REJECTED) continue; // the Link is already rejected
			if (n1 != lnk->nodes.front()) continue; // this Node is not upstream of the link.
			
			Node* n2 = lnk->nodes.back();
			for (int i = 0, n2_LS = n2->links.size(); i < n2_LS; i++) { // loop on the links of the downstream node.
				Link* lnk2 = n2->links[i];
				if (lnk2->flag == Link::REJECTED) continue; // the Link is already rejected
				Link pth = *lnk; // copy
				pth.addForward(lnk2->nodes.back());
				for (int j = 0; j < n1_LS; j++) {
					Link* lnk3 = n1->links[j];
					if (lnk == lnk3) continue;
					if (pth.include(lnk3)) {
						lnk3->flag = Link::REJECTED;
					}
				}
			}
		}
	}
	
	for (int i = 0, nodes_size = nodes.size(); i < nodes_size; i++) {
		Node* n1 = nodes[i];
		for (int j = 0; j < n1->links.size(); ) {
			Link* lnk = n1->links[j];
			if (lnk->flag == Link::REJECTED) n1->removeLink(lnk);
			else j++;
		}
	}
	
	for (int ilnk = 0, lt_size = linkstemp.size(); ilnk < lt_size; ilnk++) {
		Link* lnk = linkstemp[ilnk];
		if (lnk->flag == Link::REJECTED) delete lnk; // rejected links are deleted.
		else links.push_back(lnk);
	}
	
	printf("links cleaning = %zu->%zu\n", linkstemp.size(), links.size());
}



int check_next_unique_connection(Link* pth) {
	// there should be only one Link between the last Node of Link to the next node.
	//   ---n*---n*---n1
	Node* n1 = pth->nodeLast(); // last Node of this path
	
	// forward Link of n1 should be unique(=1). if 0, end of track.
	std::vector<Link*> links1f = n1->getLinks(Link::NORMAL, Link::FORWARD); // forward
	if (links1f.size() != 1) return 0;
	
	//   ---n1 -?- n2
	//        edge to be checked
	
	// backward Link of n2 should be unique.
	Node* n2 = links1f[0]->nodeLast();
	std::vector<Link*> links2b = n2->getLinks(Link::NORMAL, Link::BACKWARD); // backward
	if (links2b.size() != 1) return 0;
	
	// they should be unique.
	if (links2b[0] != links1f[0]) return 0;
	
	// n1 and n2 are uniquely connected!
	//   ---n1---n2
	
	Link* l = links1f[0];
	// reject this edge, but add this Node or nodes to this path.
	if (l->type == Link::PATH) {
		//   ---n1---n*---n*---n*---n2   <- path case
		pth->add(l);
		for (int i = 0, ln_size = l->nodes.size(); i < ln_size; i++) {
			Node* n = l->nodes[i];
			n->addLink(pth);
			pth->add(n);
		}
	}
	else {
		//
		pth->add(n2);
		n2->addLink(pth);
	}
	links1f[0]->flag = Link::REJECTED;

	// check the next connection.
	check_next_unique_connection(pth);
	
	return 1;
}


void make_unique_connection(Node* n1) {
	
	// count the number of forward links of this node. If it is not equal 1, continue.
	std::vector<Link*> links1f = n1->getLinks(Link::NORMAL, 1); // forward, meaning n1 is the first Node of the link.
	if (links1f.size() != 1) return;
	
	// count the number of backward links of the linked node. If it is 1, uniquely connected.
	Link* lnk = links1f[0];
	Node* n2 = lnk->nodeLast();
	std::vector<Link*> links2b = n2->getLinks(Link::NORMAL, -1); // backward
	if (links2b.size() != 1) return;

	if (links2b[0] != links1f[0]) return;
	
	// n1 and n2 are uniquely connected!
	//       n1---n2
	//       n1---n2---
	//       n1---n2===
	
	Link* pth;
	
	if (lnk->type == Link::PATH) {
		// If the Link is already a path and not an edge, use the Link itself.
		// This is for the secondary iteration.
		pth = lnk;
	}
	else {
		pth = new Link; // make a path
		*pth = *lnk; // copy
		pth->type = Link::PATH; // Differenciation between edge candidate or uniconnected path.
		lnk->flag = Link::REJECTED; // already used
		links.push_back(pth);
		n1->addLink(pth);
		n2->addLink(pth);
	}
	
	// check the next connection
	int ret = check_next_unique_connection(pth);
}

void make_unique_connection() {
	
	printf("make unique_connection\n");
	// nodes are sorted by v[0] (ipl).
	
	for (int i = 0, nodes_size = nodes.size(); i < nodes_size; i++) {
		//if (i%100 == 0) printf("i = %d / %zu\r", i, nodes.size());
		
		Node* n1 = nodes[i];
		make_unique_connection(n1);
	}
	
	printf("%zu nodes\n", nodes.size());
	printf("%zu links\n", links.size());
}



int enum_next(Link* l1, int direction, std::vector<Link*>& paths, int depth) {
	static int depth_max = 6;
	
	// If the depth of branching is more than depth_max, stop searching the following nodes.
	// This is to avoid infinit combination of possible paths.
	depth++;
	if (depth >= depth_max) return 1;

	Node* n1 = direction == Link::FORWARD ? l1->nodeLast() : l1->nodeFirst();
	
	std::vector<Link*> linksF = n1->getLinks(Link::NORMAL, direction);
	
	// If this is the end of path.
	if ( linksF.empty() ) return 1;
	
	// If forward link exist...
	for (int j = linksF.size() - 1; j >= 0; j--) { // count down because if nlink>2, l1 must be copied.
		Link* l2 = linksF[j];
		if (j == 0) {
			// If this is the end of branching path. Stop recursive function call.
			if (linksF.size() == 1) if (linksF[0]->type == Link::PATH) return paths.size();
			
			// Otherwise continue searching the connection.
			l1->add(l2);
			enum_next(l1, direction, paths, depth);
		}
		if (j != 0) {
			// Additional branching. Create new path and copy.
			Link* l3 = new Link;
			*l3 = *l1; // copy
			l3->add(l2);
			paths.push_back(l3);
			enum_next(l3, direction, paths, depth);
		}
		
	}
	return 1;
}


int enum_path_a_node(Node* n1, int direction, std::vector<Link*>& paths) {
	// Calculate all possible paths which are not uniquely defined.
	// vector paths will be filled with the list of possible paths.
	
	std::vector<Link*> linksF = n1->getLinks(Link::NORMAL, direction);
//	printf("note ID = %d, direction %d\n", ((EdbSegP*) n1->getData())->ID(), direction);
//	for (int i = 0; i < n1->links.size(); i++) {
//		Link* l = n1->links[i];
//		printf("  %d  %d -> %d\n", l->flag, ((EdbSegP*) l->nodeFirst()->getData())->ID(), ((EdbSegP*) l->nodeLast()->getData())->ID());
//	}
//	printf("linksF size = %zu\n", linksF.size());
	
	int depth = 0;
	for (int j = 0, lF_size = linksF.size(); j < lF_size; j++) {
		Link* l = linksF[j];
		
		Link* l2 = new Link;
		*l2 = *l;
		paths.push_back(l2);
		enum_next(l2, direction, paths, depth);
	}
	return paths.size();
}




int resolve_multi_connected() {
	// Resolve multi connected paths.
	// 1. calculate all possible paths.
	// 2. find best path accoding to what eval_path() will return.
	
	printf("resolving multi connections\n");
	
	int nlinkorg = links.size();
	
	for (int i = 0, nodes_size = nodes.size(); i < nodes_size; i++) {
		
		//if (i%1000 == 0) printf("%d / %zu\r", i, nodes.size());
		Node* n1 = nodes[i];
		
		// check if this Node has multiple connections
		std::vector<Link*> linksF = n1->getLinks(Link::NORMAL, Link::FORWARD);
		std::vector<Link*> linksB = n1->getLinks(Link::NORMAL, Link::BACKWARD);
		
		// If not, continue.
		if (linksF.size() < 2 && linksB.size() < 2) continue;
		
		//EdbSegP* s = (EdbSegP*) n1->data;
		//printf("ipl %d, id %d has multiple connection\n", s->Plate(), s->ID());
		
		// find possible path for this Node to forward and backward
		std::vector<Link*> paths;
		enum_path_a_node(n1, Link::FORWARD, paths);
		if (linksF.empty()) enum_path_a_node(n1, Link::BACKWARD, paths);
		
		// opposite way from the last Node of links, in order to pick all linked nodes and paths.
		if ( paths.empty() ) continue;
		int npaths = paths.size();
		
		for (int j = 0; j < npaths; j++) {
			std::vector<Link*> pathsTemp;
			Node* nodeLast = paths[j]->nodeLast();
			enum_path_a_node(paths[j]->nodeLast(), Link::BACKWARD, pathsTemp);
			
			for (int k = 0, pT_size = pathsTemp.size(); k < pT_size; k++) {
				Link* lk = pathsTemp[k];
				// check if this path is included in other paths.
				int flag_included = 0;
				for (int m = 0, paths_size = paths.size(); m < paths_size; m++) {
					Link* lm = paths[m];
					if (lm->include(lk)) flag_included = 1;
				}
				if (flag_included) delete lk;
				else paths.push_back(lk);
			}
			
		}
		
		// now paths has all possible paths (almost)
		// choose the best one.
		// calculate test statistic to choose the best one
		Link* pathBest = NULL;
		float valueBest = 1e9;
		for (int j = 0, paths_size = paths.size(); j < paths_size; j++) {
			Link* l = paths[j];
			//for (int k = 0; k < l->nodes.size(); k++) {
			//	printf("%d%c", ((EdbSegP*) l->nodes[k]->getData())->ID(), k == l->nodes.size()-1?' ':'-');
			//}
			
			if (l->nodes.size() > 1) {
				float value = eval_path(l);
			//	printf(" value=%.2f", value);
				if (valueBest > value) {
					valueBest = value;
					pathBest = l;
				}
			}
			//printf("\n");
			
		}
		
			
		// keep the best one as a new path
		Link* lbest = new Link;
		*lbest = *pathBest;
		lbest->type = Link::PATH;
		links.push_back(lbest);
		// release memory
		for (int j = 0, paths_size = paths.size(); j < paths_size; j++) delete paths[j];
		
		
		// If the first Node has a backward link, merge it
		std::vector<Link*> linksB2 = lbest->nodeFirst()->getLinks(Link::NORMAL, Link::BACKWARD);
		if (linksB2.size() == 1) {
			lbest->add(linksB2[0]);
			linksB2[0]->flag = Link::REJECTED;
		}
		
		
		// If the last Node has a forward link, merge the forward one into upstream one.
		std::vector<Link*> linksF2 = lbest->nodeLast()->getLinks(Link::NORMAL, Link::FORWARD);
		if (linksF2.size() == 1) {
			if (lbest->add(linksF2[0]));
			linksF2[0]->flag = Link::REJECTED;
		}
		
		//printf("best path is ");
		//for (int k = 0; k<lbest->nodes.size(); k++) printf("%d%c", ((EdbSegP*) lbest->nodes[k]->getData())->ID(), k==lbest->nodes.size()-1?' ':'-');
		//printf("\n");
		
		// reject all other links related to the best path
		for (int j = 0, lbn_size = lbest->nodes.size(); j < lbn_size; j++) {
			Node* n = lbest->nodes[j];
			
			for (int k = 0, n_links_size = n->links.size(); k < n_links_size; k++) {
				if (n->links[k] == lbest) continue;
				n->links[k]->flag = Link::REJECTED;
			}
			if (std::find(n->links.begin(), n->links.end(), lbest) == n->links.end()) n->links.push_back(lbest);
		}
		
		if (!lbest->include(n1)) i--; // if the best path doesn't include the very Node of this loop, repeat again.
		
	}
	
	// return delta of number of links.
	return links.size() - nlinkorg;
	
	
}



void reconnect_paths() {
	
	int nLink = links.size();
	
	printf("Reconnect paths\n");
	
	for (int iLink = 0; iLink < nLink; iLink++) {
		//if (iLink%10000 == 0) printf("i = %d / %d\r", iLink, nLink);
		Link* l = links[iLink];
		if (l->flag == Link::REJECTED) continue;
		
		int flag_modified = 0;
		
		// Check backward
		
		Node* n1 = l->nodeFirst();
		EdbSegP* s1 = (EdbSegP*) n1->getData();
		int ipl0 = s1->Plate();
		float r[3] = {0.1, 20, 20};
		float bestChi2 = 1e9;
		Link* bestLink = NULL;
		for (int dpl = -1; dpl >= -4; dpl--) {
			float v[3];
                        int s1Pdpl = s1->Plate() + dpl;
			v[0] = s1Pdpl;
                        float ztable_s1Z = ztable[s1Pdpl] - s1->Z();
			v[1] = s1->X() + s1->TX()*ztable_s1Z;
			v[2] = s1->Y() + s1->TY()*ztable_s1Z;
			
			std::vector<Node*>& neighbors = ht->getNeighbors(v, r);
			int nn = neighbors.size();
			for (int j = 0; j < nn; j++) {
				Node* n2 = neighbors[j];
				if (n1 == n2) continue;
				if (n1 < n2) continue;
				// check if n2 has backward link
				std::vector<Link*> linksB = n2->getLinks(Link::NORMAL, Link::BACKWARD);
				if (linksB.size() != 1) continue;
				
				// if this pair is within the criteria, connect.
				float chi2;
				if (eval_edge(n2, n1, &chi2) == 1) {
					Link ll;
					ll = *l;
					ll.add(n2);
					chi2 = eval_path(&ll);
					if (chi2 < bestChi2) {
						bestChi2 = chi2;
						bestLink = linksB[0];
					}
				}
			}
		}
		
		// If the first Node has a backward link, merge  it to this.
		if (bestLink != NULL) {
			l->add(bestLink);
			bestLink->flag = Link::REJECTED;
			flag_modified++;
		}

		// Check forward
		
		s1 = (EdbSegP*) l->nodeLast()->getData();
		ipl0 = s1->Plate();
		bestChi2 = 1e9;
		bestLink = NULL;
		for (int dpl = 1; dpl <= 4; dpl++) {
			float v[3];
                        int s1Pdpl = s1->Plate() + dpl;
			v[0] = s1Pdpl;
                        float ztable_s1Z = ztable[s1Pdpl] - s1->Z();
			v[1] = s1->X() + s1->TX()*ztable_s1Z;
			v[2] = s1->Y() + s1->TY()*ztable_s1Z;
			
			std::vector<Node*>& neighbors = ht->getNeighbors(v, r);
			int nn = neighbors.size();
			for (int j = 0; j < nn; j++) {
				Node* n2 = neighbors[j];
				if (n1 == n2) continue;
				if (n1 > n2) continue;
				// check if n2 has forward link
				std::vector<Link*> linksF = n2->getLinks(Link::NORMAL, Link::FORWARD);
				if (linksF.size() != 1) continue;
				
				// if this pair is within the criteria, connect.
				float chi2;
				if (eval_edge(n2, n1, &chi2) == 1) {
					Link ll;
					ll = *l;
					ll.add(n2);
					chi2 = eval_path(&ll);
					if (chi2 < bestChi2) {
						bestChi2 = chi2;
						bestLink = linksF[0];
					}
				}
			}
		}
		
		// If the last Node has a forward link, merge the best one into this.
		if (bestLink != NULL) {
			l->add(bestLink);
			bestLink->flag = Link::REJECTED;
			flag_modified++;
		}
		
		
		// if there is a change, set links to nodes.
		// reject all other links related to the this path
		if (flag_modified) {
			for (int j = 0, ln_size = l->nodes.size(); j < ln_size; j++) {
				Node* n = l->nodes[j];
				for (int k = 0, n_links_size = n->links.size(); k < n_links_size; k++) {
					if (n->links[k] == l) continue;
					n->links[k]->flag = Link::REJECTED;
				}
				if (std::find(n->links.begin(), n->links.end(), l) == n->links.end()) n->links.push_back(l);
			}
			
			iLink--; // repeat this path again in order to search the continuation.
		}
		
		
	}
	
}



void reconnect_single_nodes() {
	
	int nLink = links.size();
	
	printf("Search for single nodes at hole of each path\n");
	
	for (int iLink = 0; iLink < nLink; iLink++) {
		//if (iLink%10000 == 0) printf("i = %d / %d\r", iLink, nLink);
		Link* l = links[iLink];
		if (l->flag == Link::REJECTED) continue;
		
		int flag_modified = 0;
		
		// Check backward
		
		Node* n1 = l->nodes[0]; // nearest segment
		
		for (int ipl = iplMin; ipl <= iplMax; ipl++) {
			
			// Check if there is a segment on this plate.
			Node* n2 = NULL;
			for (int i = 0, ln_size = l->nodes.size(); i < ln_size; i++) {
				EdbSegP* ss = (EdbSegP*) l->nodes[i]->getData();
				if (ipl == ss->Plate()) {
					n2 = l->nodes[i];
					n1 = n2; // update nearest segment
				}
			}
			
			if (n2) continue; // If a segment on this plate, continue;
			
			// Search for a segment around
			
			float r[3] = {0.1, 20, 20};
			float bestChi2 = 1e9;
			Node* bestNode = NULL;
			float v[3];
			v[0] = ipl;
			EdbSegP* s1 = (EdbSegP*) n1->getData();
                        float ztable_s1Z = ztable[ipl] - s1->Z();
			v[1] = s1->X() + s1->TX()*ztable_s1Z;
			v[2] = s1->Y() + s1->TY()*ztable_s1Z;
			
			std::vector<Node*>& neighbors = ht->getNeighbors(v, r);
			int nn = neighbors.size();
			for (int j = 0; j < nn; j++) {
				Node* n = neighbors[j];
				
				// check if n2 has normal links
				std::vector<Link*> linksN = n->getLinks(Link::NORMAL);
				if ( !linksN.empty() ) continue;
				
				// if this pair is within the criteria, connect.
				float chi2;
				if (eval_edge(n, n1, &chi2) == 1) {
					if (chi2 < bestChi2) {
						bestChi2 = chi2;
						bestNode = n;
					}
				}
			}
			
			if (bestNode == NULL) continue;
			
			l->add(bestNode);
			bestNode->addLink(l);
		}
	}
	
}




void write_tracks() {
	
	printf("Write tracks into file.\n");
	
	TFile f("linked_tracks.root", "recreate");
	TTree* tracks = new TTree("tracks", "tracks");

	EdbTrackP*    track = new EdbTrackP(8);
	EdbSegP*      tr = new EdbSegP;
	TClonesArray* segments  = new TClonesArray("EdbSegP");
	TClonesArray* segmentsf = new TClonesArray("EdbSegP");

	int   nseg, trid, npl, n0;
	float w = 0.;
	float xv, yv;

	tracks->Branch("trid", &trid,     "trid/I");
	tracks->Branch("nseg", &nseg,     "nseg/I");
	tracks->Branch("npl",  &npl,      "npl/I");
	tracks->Branch("n0",   &n0,       "n0/I");
	tracks->Branch("xv",   &xv,       "xv/F");
	tracks->Branch("yv",   &yv,       "yv/F");
	tracks->Branch("w",    &w,        "w/F");
	tracks->Branch("t.",   "EdbSegP", &tr, 32000, 99);
	tracks->Branch("s",    &segments);
	tracks->Branch("sf",   &segmentsf);
	
	int itrk = 0;
	for (int i = 0, links_size = links.size(); i < links_size; i++) {
		Link* l = links[i];
		if (l->flag == Link::REJECTED) continue;
		
		EdbSegP* s1 = (EdbSegP*) l->nodeFirst()->getData();
		EdbSegP* s2 = (EdbSegP*) l->nodeLast()->getData();
		// tr = s1; //  <-- source of bug!! 2018/5/14
		tr->SetID(itrk);
		
		*tr = *s1;
		
		trid = tr->ID();
		nseg = l->nodes.size();
		npl  = s2->Plate() - s1->Plate() + 1;
		n0   = npl - nseg;
		
		segments->Clear("C");
		segmentsf->Clear("C");
		
		w = 0;
		for (int j = 0; j < nseg; j++) {
			w += ((EdbSegP*) l->nodes[j]->getData())->W();
		}
		
		EdbSegP *s = 0, *sf = 0;
		for (int is = 0; is < nseg; is++) {
			s = (EdbSegP*) l->nodes[is]->getData();
			if (s) new((*segments)[is]) EdbSegP(*s);
			sf = s;
			if (sf) new((*segmentsf)[is]) EdbSegP(*sf);
		}

		tr->SetVid(0, itrk);  // put track counter in t.eVid[1]
		itrk++;
		tracks->Fill();
	}
	
	tracks->Write();
	f.Close();
}


void display() {
	
	
	int argc = 1;
	char temp[10] = "tracking";
	
	char* argv[2];
	argv[0] = temp;
	
	TRint rint("display", &argc, argv);
	
	TEveManager::Create();
	TGLViewer* glv = gEve->GetDefaultGLViewer();
	
	for (int i = 0, nodes_size = nodes.size(); i < nodes_size; i++) {
		EdbSegP* s = (EdbSegP*) nodes[i]->getData();
		
		float z0 = s->Z();
		
		float x1, y1, z1, x2, y2, z2;
		s->PropagateTo(z0 - 100);
		x1 = s->X();
		y1 = s->Y();
		z1 = s->Z();
		
		s->PropagateTo(z0 + 100);
		x2 = s->X();
		y2 = s->Y();
		z2 = s->Z();
		
		s->PropagateTo(z0);
		
		int nforward = 0;
		int nbackward = 0;
		for (int j = 0, nl_size = nodes[i]->links.size(); j < nl_size; j++) {
			if (nodes[i]->links[j]->flag != Link::NORMAL) continue;
			if (nodes[i]->links[j]->nodeFirst() == nodes[i]) nforward++;
			if (nodes[i]->links[j]->nodeLast() == nodes[i]) nbackward++;
		}
		
		int branching = nforward >= 2 || nbackward >= 2;
		
		TEveLine* l = new TEveLine;
		l->SetLineColor(branching ? kRed : kYellow);
		l->SetLineWidth(2);
		l->SetTitle(Form("%d %d %f %f %f %f %f", s->Plate(), s->ID(), s->X(), s->Y(), s->Z(), s->TX(), s->TY()));
		l->SetNextPoint(x1, y1, z1);
		l->SetNextPoint(x2, y2, z2);
		gEve->AddElement(l);

	}

	for (int i = 0, links_size = links.size(); i < links_size; i++) {
		Link* l = links[i];
		
		if (l->flag != Link::NORMAL) continue;
		
		TEveLine* ln = new TEveLine;
		
		int color = l->type == Link::EDGE ? kOrange : kGreen;
		ln->SetLineColor(color);
		for (int j = 0, ln_size = l->nodes.size(); j < ln_size; j++) {
			EdbSegP* s = (EdbSegP*) l->nodes[j]->getData();
			float x = s->X();
			float y = s->Y();
			float z = s->Z();
			ln->SetNextPoint(x, y, z);
		}
		gEve->AddElement(ln);
	}
	
	gEve->Redraw3D(kTRUE);
	
	rint.Run();
}


void init_parameters() {
	
	TEnv setting("tracking.env");
	
	
}

int main(int argc, char* argv[]) {
	
	if (argc < 2) return 1;
	
	char* filename = argv[1];
	TCut cut = "1";
	if (argc == 3) {
		cut = argv[2];
	}
	
	
	ProcessTimeChecker pc;
	pc.AddEntry("Read file");

	TString fname = filename;
	if (fname.Contains(".def")) {
		read_nodes_lnkdef(filename);
	}
	else read_nodes_test(filename, cut);
	
	
	pc.AddEntry("Fill hash table");

	
	TStopwatch sw;
	printf("Start stopwatch.\n");
	sw.Start();
	
	float xmin =  1e9;
	float xmax = -1e9;
	float ymin =  1e9;
	float ymax = -1e9;
	
	// initialize ztable[ipl]
	for (int i = 0; i < 200; i++) ztable[i] = 0;
	
	for (int i = 0, nodes_size = nodes.size(); i < nodes_size; i++) {
		EdbSegP* s = (EdbSegP*) nodes[i]->getData();

                int sPlate = s->Plate();

                float sX = s->X();
                float sY = s->Y();

		if (iplMin > sPlate) iplMin = sPlate;
		if (iplMax < sPlate) iplMax = sPlate;
		if (xmin > sX) xmin = sX;
		if (xmax < sX) xmax = sX;
		if (ymin > sY) ymin = sY;
		if (ymax < sY) ymax = sY;
		ztable[sPlate] = s->Z();
	}
	xmin -= 1; xmax += 1; ymin -= 1; ymax += 1;
	
	printf("ipl %d - %d\n", iplMin, iplMax);
	
	float cellsize = 20; // 50 micron
	
	ht = new hashtable3d;
	ht->setCells(iplMax - iplMin + 1, iplMin - 0.5, iplMax + 0.5,
	             floor((xmax - xmin)/cellsize) + 1, xmin, xmax,
	             floor((ymax - ymin)/cellsize) + 1, ymin, ymax);
	
	ht->fillCells(nodes);
	
	pc.AddEntry("make_edges()");

	// Make any two combination of nodes. If accepted by eval_edge(), it is stored as a link.
	make_edges();
//	make_edges_multithread();
	
	pc.AddEntry("make_unique_connection()");

	
	int iloop = 0;
	int ret = 1;
	while (iloop++ < 3 && ret) {
		make_unique_connection();
		ret = resolve_multi_connected(); // ret is number of new paths after resolving multi connection.
		//printf("loop %d, Nnew paths=%d\n", iloop, ret);
	}
	
	pc.AddEntry("reconnect_paths()");
	reconnect_paths();
	
	printf("process end\n");
	int nActiveLinks = 0;
	for (int i = 0, links_size = links.size(); i < links_size; i++)
            if (links[i]->flag == Link::NORMAL) nActiveLinks++;
	printf("nActiveLinks = %d\n", nActiveLinks);
	
	sw.Stop();
	sw.Print();
	
	pc.AddEntry("write_tracks()");
	write_tracks();
	
	pc.End();
	pc.Print();
	
	//display();
	
	return 0;
}

